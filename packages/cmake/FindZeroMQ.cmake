include(init)

find_package(ZeroMQ REQUIRED CONFIG
    PATHS "${EXTERNALS_DIR}/zeromq/share/cmake/ZeroMQ"
    NO_DEFAULT_PATH)
