include(init)

find_package(cppzmq REQUIRED CONFIG
    PATHS "${EXTERNALS_DIR}/cppzmq/share/cmake/cppzmq"
    NO_DEFAULT_PATH)
