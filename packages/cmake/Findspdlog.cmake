include(init)

find_package(spdlog REQUIRED CONFIG
    PATHS "${EXTERNALS_DIR}/spdlog/lib/cmake/spdlog"
    NO_DEFAULT_PATH)
