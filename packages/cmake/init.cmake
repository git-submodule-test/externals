if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif ()

if (NOT BUILD_TYPE)
    string(TOLOWER ${CMAKE_BUILD_TYPE} BUILD_TYPE)
endif ()

if (NOT PLATFORM_ID)
    message(FATAL "you must specify a platform ID. Example: -DPLATFORM_ID=linux-fedora31")
endif ()

if (NOT EXTERNALS_DIR)
    set(EXTERNALS_DIR
        "${CMAKE_CURRENT_LIST_DIR}/../${PLATFORM_ID}/${BUILD_TYPE}"
        CACHE PATH "Platform and build-type specific external dependencies")
    message(STATUS "Using external dependencies from: ${EXTERNALS_DIR}")
endif ()
